#############################
### generic configuration ###
#############################

[batconfig]
multiprocessing = no

####################
### prerun scans ###
####################

[checkXML]
type    = prerun
module  = bat.prerun
method  = searchXML
priority = 100

#[verifybz2]
#type    = prerun
#module  = bat.prerun
#method  = verifyBZ2
#priority = 1

#[verifygzip]
#type    = prerun
#module  = bat.prerun
#method  = verifyGzip
#priority = 1

[verifytext]
type    = prerun
module  = bat.prerun
method  = verifyText
priority = 2

[verifygraphics]
type    = prerun
module  = bat.prerun
method  = verifyGraphics
magic   = bmp
priority = 1

####################
### unpack scans ###
####################

[7z]
type    = unpack
module  = bat.fwunpack
method  = searchUnpack7z
priority = 1
magic = 7z
noscan   = text:xml:graphics:pdf:bz2:gzip:lrzip

[ar]
type    = unpack
module  = bat.fwunpack
method  = searchUnpackAr
priority = 2
magic = ar
noscan   = text:xml:graphics:pdf:bz2:gzip:lrzip

#[arj]
#type    = unpack
#module  = bat.fwunpack
#method  = searchUnpackARJ
#priority = 1
#magic = arj
#noscan   = text:xml:graphics:pdf:bz2:gzip:lrzip

[base64]
type    = unpack
module  = bat.fwunpack
method  = searchUnpackBase64
priority = 0
noscan   = xml:graphics:binary:pdf:compressed

[byteswap]
type    = unpack
module  = bat.fwunpack
method  = searchUnpackByteSwap
priority = 100
noscan   = xml:graphics:pdf:compressed

[bzip2]
type    = unpack
module  = bat.fwunpack
method  = searchUnpackBzip2
priority = 1
magic = bz2
noscan   = text:xml:graphics:pdf:gzip:lrzip

[cab]
type    = unpack
module  = bat.fwunpack
method  = searchUnpackCab
priority = 1
magic = cab
noscan   = text:xml:graphics:pdf:compressed

[compress]
type     = unpack
module   = bat.fwunpack
method   = searchUnpackCompress
priority = 1
magic    = compress
noscan   = text:xml:graphics:pdf

[cpio]
type    = unpack
module  = bat.fwunpack
method  = searchUnpackCpio
priority = 2
magic = cpio1:cpio2:cpio3:cpiotrailer
noscan   = text:xml:graphics:pdf:compressed

[cramfs]
type    = unpack
module  = bat.fwunpack
method  = searchUnpackCramfs
priority = 3
magic = cramfs
noscan   = text:xml:graphics:pdf:compressed

[exe]
type    = unpack
module  = bat.fwunpack
method  = searchUnpackExe
priority = 2
noscan   = text:xml:graphics:pdf:compressed

[ext2fs]
type    = unpack
module  = bat.fwunpack
method  = searchUnpackExt2fs
priority = 3
magic = ext2
noscan   = text:xml:graphics:pdf:compressed

[gif]
type    = unpack
module  = bat.fwunpack
method  = searchUnpackGIF
priority = 0
magic = gif87:gif89
noscan   = text:xml:jpeg:bmp:pdf:png:compressed

[gzip]
type    = unpack
module  = bat.fwunpack
method  = searchUnpackGzip
priority = 1
magic = gzip
noscan   = text:xml:graphics:pdf:bz2:lrzip

#[ico]
#type    = unpack
#module  = bat.fwunpack
#method  = searchUnpackIco
#priority = 0
#magic = ico
#noscan   = text:xml:jpeg:bmp:pdf:gif:png:compressed

[installshield]
type    = unpack
module  = bat.fwunpack
method  = searchUnpackInstallShield
priority = 1
magic = installshield
noscan   = text:xml:graphics:pdf:bz2:gzip:lrzip

[iso9660]
type    = unpack
module  = bat.fwunpack
method  = searchUnpackISO9660
priority = 4
magic = iso9660
noscan   = text:xml:graphics:pdf:compressed

#[java_serialized]
#type    = unpack
#module  = bat.fwunpack
#method  = searchUnpackJavaSerialized
#priority = 2
#magic = java_serialized
#noscan   = text:xml:graphics:pdf:bz2:gzip:lrzip

[jffs2]
type    = unpack
module  = bat.fwunpack
method  = searchUnpackJffs2
priority = 1
magic = jffs2_le
noscan   = text:xml:graphics:pdf:compressed

[lrzip]
type    = unpack
module  = bat.fwunpack
method  = searchUnpackLRZIP
priority = 1
magic = lrzip
noscan   = text:xml:graphics:pdf:bz2:gzip:lzip

[lzip]
type    = unpack
module  = bat.fwunpack
method  = searchUnpackLzip
priority = 1
magic = lzip
noscan   = text:xml:graphics:pdf:bz2:gzip:lrzip

[lzma]
type    = unpack
module  = bat.fwunpack
method  = searchUnpackLZMA
priority = 1
magic = lzma_alone:lzma_alone_alt
noscan   = text:xml:graphics:pdf:bz2:gzip:lrzip

[lzo]
type    = unpack
module  = bat.fwunpack
method  = searchUnpackLzo
priority = 1
magic = lzo
noscan   = text:xml:graphics:pdf:bz2:gzip:lrzip

[pdf_unpack]
type    = unpack
module  = bat.fwunpack
method  = searchUnpackPDF
priority = 10
magic = pdf:pdftrailer
noscan   = text:xml:graphics:compressed

[png]
type    = unpack
module  = bat.fwunpack
method  = searchUnpackPNG
priority = 0
magic = png:pngtrailer
noscan   = text:xml:jpeg:bmp:pdf:gif:compressed

[rar]
type    = unpack
module  = bat.fwunpack
method  = searchUnpackRar
priority = 1
magic = rar
noscan   = text:xml:graphics:pdf:bz2:gzip:lrzip

[rpm]
type    = unpack
module  = bat.unpackrpm
method  = searchUnpackRPM
priority = 2
magic = rpm:gzip:xz:xztrailer
noscan   = text:xml:graphics:pdf:compressed

[squashfs]
type    = unpack
module  = bat.fwunpack
method  = searchUnpackSquashfs
priority = 1
magic = squashfs1:squashfs2:squashfs3:squashfs4:squashfs5:squashfs6
noscan   = text:xml:graphics:pdf:compressed

[swf]
type    = unpack
module  = bat.fwunpack
method  = searchUnpackSwf
priority = 0
magic = swf
noscan   = text:xml:jpeg:bmp:pdf:png:gif:compressed

[tar]
type    = unpack
module  = bat.fwunpack
method  = searchUnpackTar
priority = 3
magic = tar1:tar2
noscan   = text:xml:graphics:pdf:bz2:gzip:lrzip

[ubifs]
type    = unpack
module  = bat.fwunpack
method  = searchUnpackUbifs
priority = 3
magic = ubifs
noscan   = text:xml:graphics:pdf:compressed

[upx]
type    = unpack
module  = bat.fwunpack
method  = searchUnpackUPX
priority = 3
noscan   = text:xml:graphics:pdf:compressed

[xz]
type    = unpack
module  = bat.fwunpack
method  = searchUnpackXZ
priority = 1
magic = xz:xztrailer
noscan   = text:xml:graphics:pdf:bz2:gzip:lrzip

[yaffs]
type    = unpack
module  = bat.fwunpack
method  = searchUnpackYaffs2
priority = 2
noscan   = text:xml:graphics:pdf:compressed

[zip]
type    = unpack
module  = bat.fwunpack
method  = searchUnpackZip
priority = 1
magic = zip
noscan   = text:xml:graphics:pdf:bz2:gzip:lrzip

##################
### leaf scans ###
##################

[architecture]
type    = program
module  = bat.checks
method  = scanArchitecture
noscan  = text:xml:graphics:pdf:compressed

[busybox-version]
type    = program
module  = bat.busyboxversion
method  = busybox_version
noscan  = text:xml:graphics:pdf:compressed

[dproxy]
type    = program
module  = bat.checks
method  = searchDproxy
noscan  = text:xml:graphics:pdf:compressed

[ez-ipupdate]
type    = program
module  = bat.checks
method  = searchEzIpupdate
noscan  = text:xml:graphics:pdf:compressed

[forges]
type    = program
module  = bat.checks
method  = scanForges
xmloutput = forgesPrettyPrint
noscan  = graphics:compressed

[iproute]
type    = program
module  = bat.checks
method  = searchIproute
noscan  = text:xml:graphics:pdf:compressed

[iptables]
type    = program
module  = bat.checks
method  = searchIptables
noscan  = text:xml:graphics:pdf:compressed

[kernelchecks]
type    = program
module  = bat.kernelanalysis
method  = kernelChecks
xmloutput = xmlprettyprint
noscan  = text:xml:graphics:pdf:compressed

[kernelmodulelicense]
type    = program
module  = bat.kernelanalysis
method  = analyseModuleLicense
noscan  = text:xml:graphics:pdf:compressed

[libs]
type    = program
module  = bat.checks
method  = searchDynamicLibs
xmloutput = dynamicLibsPrettyPrint
noscan  = text:xml:graphics:pdf:compressed

[libusb]
type    = program
module  = bat.checks
method  = searchLibusb
noscan  = text:xml:graphics:pdf:compressed

[licenses]
type    = program
module  = bat.checks
method  = scanLicenses
xmloutput = licensesPrettyPrint
noscan    = compressed

[loadlin]
type    = program
module  = bat.checks
method  = searchLoadLin
noscan  = text:xml:graphics:pdf:compressed

[pdf]
type    = program
module  = bat.checks
method  = scanPDF
xmloutput = pdfPrettyPrint
noscan  = text:xml:graphics:compressed

#[ranking]
#type    = program
#module  = bat.ranking
#method  = searchGeneric
#xmloutput = xmlprettyprint
#envvars = BAT_SQLITE_AVG=/home/armijn/master/avg:BAT_SQLITE_STRINGSCACHE=/home/armijn/master/stringscache:BAT_SQLITE_DB=/home/armijn/master/master
#noscan  = text:xml:graphics:pdf:compressed

[redboot]
type    = program
module  = bat.checks
method  = searchRedBoot
noscan  = text:xml:graphics:pdf:compressed

[uboot]
type    = program
module  = bat.checks
method  = searchUBoot
noscan  = text:xml:graphics:pdf:compressed

[vsftpd]
type    = program
module  = bat.checks
method  = searchVsftpd
noscan  = text:xml:graphics:pdf:compressed

[wireless-tools]
type    = program
module  = bat.checks
method  = searchWirelessTools
noscan  = text:xml:graphics:pdf:compressed

[wpa-supplicant]
type    = program
module  = bat.checks
method  = searchWpaSupplicant
noscan  = text:xml:graphics:pdf:compressed
