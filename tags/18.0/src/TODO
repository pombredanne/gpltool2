TODO:

file systems:
* FAT16 (use python-construct)
* FAT32
* MBR (use python-construct)
* NTFS
* ubifs (unubi was removed from mtd-utils)
* more generic YAFFS2 support
* squashfs as described at http://www.devttys0.com/2011/08/extracting-non-standard-squashfs-images/

compression/executables:
* E00
* add special 7z case and test with D-Link firmware DSL-524T_FW_V3.00B01T02.UK-A.20060621.zip
* better support for ZIP file comments http://www.pkware.com/documents/casestudies/APPNOTE.TXT
* use python-pefile for PE analysis
* NBG5615 uImage
* ncompress on Ubuntu gives false positives: try to filter these out

dynamic ELF scanning
* take RPATH into account
* add more functions from LSB and other standards

queueing system:
* let top level script also read from a queue
* add script to add files to and remove files from scanning queue

GUI:
* rewrite to PyQt or PySide to take advantage of better rendering engine (webkit)
* add reports to GUI for distribution checks
* move detailed function name reports from overview to function name report
* rework reporting of duplicate files
* guireports.py: report all names for a certain checksum (for example if there are copies of a file under a different name)

error handling:
* better handle errors to give users a better indication if something went wrong

database creation:
* handle embedded archives (ZIP files, tarballs, etc.)
* handle patches (both normal and unified)
* import licensing information from SPDX files
* extract more information from Linux kernel, including values from __ATTR and friends, as far as possible

ranking:
* combine string identifiers and function names to report most likely version
* use (directly) assigned identifiers to determine the most likely version
* use version information to report most likely licenses
* use macholib to analyse Mach-O binaries (MacOS X)
* if there are more results for file for a line (different line numbers), combine the two results and put the line numbers in a list (first do research to see if this makes sense)

busybox scanning:
* clean up finding ranges of applets in extract_configuration. It should be possible to do this less kludgy.

HTML generation:
* finish function name reporting (Java, Linux kernel)
* rework variable name reporting
* clean up/rewrite/make it easier to use
* add license information if available

misc:
* replay script to unpack firmware based on only the pickle with unpacking data
* add configuration option to set temporary directory prefix for all scans
* add piecharts/reports per directory that summarise what is inside a directory (recursively, perhaps only if something was unpacked)
* add per scan debug to allow better custom debugging
* test on latest OpenSUSE
* check dependencies for Ubuntu 14.04 LTS
* test on latest Debian, make sure configurations/dependencies work
* replace calls to dd, tail, head and others with truncate() where possible to prevent launching an external process
* replace hardcoded options in reporting with values of 'storetarget'
* tag webp files

BOTTLENECKS/HIGH PRIORITY:
1. ELF tagging is sometimes incorrect, so LZMA unpacker tries to extract LZMA files in vain from these ELF files, which costs a lot of time.
2. update bat-fsck.cramfs to newer version of util-linux
3. replace own counters with collections.Counter(), also for code clarity
4. better report non unique matched strings which are not clones
5. verify and tag Android resources.arsc
6. rework datastructures of ranking information (strings, function names, variables) so they are all similar
7. createbatarchive.py: add option to set unpackdir (to for example ramdisk)
8. clean up for tagKnownExtension, add more extensions if possible. Research big firmwares for this to see which extensions to focus on first.
9. research/fix priority for Minix file system
10. don't recreate ELF graphs (model for every ELF binary), but reuse graphs of dependencies. This would need a topological sort.
11. process UNSCANNED_DUPLICATE_FILES from BAT archives in createdb.py. This is needed if some of the unscanned duplicate files need to be processed.
12. better error detection in createdb.py and createbatarchive.py for corrupt bzip2 archives
13. refactor name squashing
14. find versions of matched kernel function names and report
15. store meta data about media files (PNG, GIF, MP3, etc.) in database as some of these files are quite package specific
16. better report non unique matched strings which are not clones
17 findlibs.py: fix architecture checks since right now they don't work
18. handle "disk full" and other errors
19. add support for different hashes to createbatarchive.py
20. take order of archive creation into account in createdb.py
21. add blacklist for files for database creation (like icudt46l_dat.S in chromium)
