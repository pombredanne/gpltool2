bat (18.0) unstable; urgency=high

  * fix JFFS2 and EXE 7z unpacking
  * fix for identifier.py if DEX_TMPDIR was not set
  * fix for scoring for assigned strings
  * smaller fixes (see ChangeLog)

 -- Armijn Hemel <armijn@binaryanalysis.org>  Tue, 29 Apr 2014 12:52:31 +0200

bat (17.0) unstable; urgency=high

  * add Android file system unpacking
  * fix Java aggregation
  * unescape output from jcf-dump
  * prevent I/O for big file unpacking, resulting in big speed ups
  * rename executables and configuration files
  * split identifier extraction and identifier assignment
  * optionally assign strings based on order in which they appear in the
    binary
  * much more (see ChangeLog)

 -- Armijn Hemel <armijn@binaryanalysis.org>  Wed, 2 Apr 2014 20:31:31 +0200

bat (16.0) unstable; urgency=high

  * increased fidelity for Linux kernel scanning
  * add additional piecharts for reporting
  * unpacking deduplication
  * name squashing for function names and variable names
  * big endian cramfs + old cramfs support
  * much more (see ChangeLog)

 -- Armijn Hemel <armijn@binaryanalysis.org>  Wed, 12 Mar 2014 19:20:43 +0100

bat (15.0) unstable; urgency=high

  * increased fidelity for Linux kernel scanning
  * much reduced run time for ranking scan
  * RZIP unpacking
  * JFFS2 big endian support
  * squashfs fidelity and security fixes
  * LZMA unpacking fidelity fixes
  * version chart generation with reportlab instead of pychart
  * much much more (see ChangeLog)

 -- Armijn Hemel <armijn@binaryanalysis.org>  Thu, 10 Oct 2013 13:02:43 +0200
bat (14.0) unstable; urgency=low

  * fix cramfs unpacking
  * expand dynamic ELF picture generation
  * add kernel symbol scanning
  * workarounds for crashes in squashfs unpacking
  * add 'setup' hook for program scans, where setup code can be moved into

 -- Armijn Hemel <armijn@binaryanalysis.org>  Sat, 11 May 2013 17:33:03 +0200

bat (13.0) unstable; urgency=low

  * rework inner format to save massive amounts of memory
  * merge and split several databases to use a more sane data model
  * several blacklisting fixes
  * deduplicate picture generation
  * fix JAR aggregate scanning
  * ZIP comment support
  * several new configuration directives
  * much much more (see ChangeLog)

 -- Armijn Hemel <armijn@binaryanalysis.org>  Thu, 18 Apr 2013 20:43:03 +0200

bat (12.0) unstable; urgency=low

  * add aggregate scanning
  * romfs + squashfs + jffs2 + 7z fixes
  * unlock more information (variables, Java methods + fields, etc.)
  * combine licensing from FOSSology and Ninka
  * much more (see ChangeLog)

 -- Armijn Hemel <armijn@binaryanalysis.org>  Mon, 21 Jan 2013 14:56:13 +0100

bat (11.0) unstable; urgency=high

  * fix file copying bug
  * fix BusyBox configuration bug
  * fix unnecessarily overwriting pickle result files bug

 -- Armijn Hemel <armijn@binaryanalysis.org>  Sun, 06 Jan 2013 17:26:13 +0100

bat (10.0) unstable; urgency=low

  * XZ unpacking fixes
  * split creation of images into two separate methods that can be invoked
    separately
  * introduce new configuration parameters for more granular scanning
  * new variant of squashfs
  * much more (see ChangeLog)

 -- Armijn Hemel <armijn@binaryanalysis.org>  Fri, 14 Dec 2012 18:10:13 +0100

bat (9.0) unstable; urgency=low

  * GUI fixes
  * ext2fs unpacking crash fix

 -- Armijn Hemel <armijn@binaryanalysis.org>  Thu, 4 Oct 2012 20:18:13 +0200

bat (8.0) unstable; urgency=low

  * GUI
  * romfs unpacking
  * minix fs v1 unpacking
  * function name matching
  * much more (see changelog)

 -- Armijn Hemel <armijn@binaryanalysis.org>  Tue, 22 May 2012 22:00:43 +0200

bat (7.0) unstable; urgency=low

  * Updated release, includes much new functionality. See ChangeLog in SVN repo for
    details

 -- Armijn Hemel <armijn@binaryanalysis.org>  Tue, 24 Apr 2012 00:07:43 +0200


bat (6.0) unstable; urgency=low

  * Updated release, includes much new functionality. See ChangeLog in SVN repo for
    details

 -- Armijn Hemel <armijn@binaryanalysis.org>  Thu, 12 Jan 2012 15:10:43 +0100

bat (5.0) unstable; urgency=low

  * Updated release, includes much new functionality. See ChangeLog in SVN repo for
    details

 -- Armijn Hemel <armijn@binaryanalysis.org>  Tue, 18 Oct 2011 19:13:43 +0200
