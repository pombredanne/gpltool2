#!/usr/bin/python

#-*- coding: utf-8 -*-

## Binary Analysis Tool
## Copyright 2012 Armijn Hemel for Tjaldur Software Governance Solutions
## Licensed under Apache 2.0, see LICENSE file for details

'''
This is a program for viewing results of the Binary Analysis Tool.
In the future it will also allow for a more interactive way if scanning.
'''

import sys, os, string, gzip, cPickle, bz2, tarfile, tempfile, copy
from optparse import OptionParser
import ConfigParser
import wx, wx.html, wx.lib, wx.lib.statbmp, wx.aui, wx.lib.agw.flatnotebook
import sqlite3, cgi

if sys.platform.startswith('linux'):
	try:
		import bat.bruteforcescan
		scanning = True
	except ImportError:
		scanning = False
		

helphtml = '''<html>
<head><title>Binary Analysis Tool result viewer</title></head>
<body>
<h1>Binary Analysis tool result viewer</h1>
<p>
On the left you will find a tree that can be browsed. Results can be shown by clicking on the item. The tree contains extra information to help you get to the right information straightaway:

<ul>
<li>&empty; : the file is empty</li>
<li>&rarr; : the file is a symbolic link, pointing to the file following &rarr;</li>
<li>&#x24b6; : the file is an Android Dalvik file</li>
<li>&#x24b8; : the file is a compressed file</li>
<li>&#x24b9; : the file is a directory (possibly empty)</li>
<li>&#x24bc; : the file is a graphics file</li>
<li>&#x24c7; : the file is a resources file</li>
<li>&#x24c9; : the file only contains text</li>
<li>&#x272a; : strings were found that could be matched with Open Source software</li>
</ul>
</p>

<p>
On the right there are views of the file. There are three main views of the file:

<ol>
<li>Overview</li>
<li>String matches view</li>
<li>Alternate view (advanced mode only)</li>
</ol>
</p>
<h2>Overview</h2>
<p>
The overview displays various types of information about the file, such as name, relative path inside a file system or compressed file, size, SHA256 checksum (if any), information about which packages were found (if available), and so on.
</p>
<p>
If licensing information was available during scanning it is also displayed here. Licensing information is currently only based on unique matches. Non-unique matches are not taken into account. The licensing information is not per se correct!
</p>
<h2>String matches view</h2>
<p>
In this window you will see a list of unique matches per package. For each unique match you will see:
<ul>
<li>file name</li>
<li>version(s)</li>
<li>line number</li>
<li>SHA256 checksum</li>
</ul>
</p>
<p>
Each line number is clickable. If there is an archive of pregenerated HTML source code files the corresponding source code file will be shown when clicking on the link.
</p>
<p>
If the characters { and } are in the version number it means that the version numbers have been collapsed for space reasons. The full version number can be reconstructed by taking the part before the { and a version number in between { and }. Example: "2.6.{0, 1, 2}" expands to "2.6.0, 2.6.1, 2.6.2".
</p>
<h2>Alternate view (advanced mode only)</h2>
<p>
In this tab you will find two different representations of the binary file:

<ol>
<li>picture view: each byte value in the binary file has been assigned a grayscale value. Bytes with value <tt>0x00</tt> are black, bytes with value <tt>0xff</tt> are white. Other bytes are assigned values in between.</li>
<li>hexdump view</li>
</ol>
</p>
<p>
By clicking somewhere on the picture the hexdump view will automatically scroll to the textual representation of the clicked byte. This will quickly allow you to view interesting boundaries.
</p>
<h1>Configuring the BAT viewer</h1>
<h2>Display filters</h2>
<p>
Each file is tagged (if possible) by the scanning process. Filters for the following file types are currently defined:

<ul>
<li>audio files</li>
<li>empty files</li>
<li>graphics files</li>
<li>PDF files</li>
<li>resource files</li>
<li>symbolic links</li>
<li>text files</li>
<li>video files</li>
</ul>
</p>
<p>
By default no filters have been applied. The filters can be accessed via Configuration &rarr; Filter Configuration.
</p>
<h2>Advanced mode</h2>
<p>
Advanced mode can be entered via Configuration &rarr; General Configuration. The box Advanced mode should be checked to enter advanced mode. As soon as advanced mode is entered extra data from the results file (if available) will be unpacked and an extra tab will appear. It is not advised to run advanced mode: it can be a big strain on resources.
</p>
<h2>Scanning mode</h2>
<p>
The scanning mode can only be used on Linux systems. On other systems it will be automatically disabled. Right now scanning mode is automatically enabled on Linux systems. This will change in the near futur and scanning mode will have to be explicitely enabled.
</p>
<p>
Before scans can be run a configuration file needs to be loaded. There are two ways this can be done:

<ol>
<li>supply it on the commandline as a parameter: <code>-c /path/to/configuration/file</code>
<li>load it via the menu Configuration &rarr; Open Configuration File
</ol>
</p>
<p>
Individual scans can be enabled and disabled via the check boxes under Configuration &rarr; Configure Scans
</p>
</body>
</html>
'''

## Specialized HTML window for handling links to pretty printed source code
## Basically just replaces 'unique:/' with the proper path to the pretty printed
## source code files.
class BATHtmlWindow(wx.html.HtmlWindow):
	def __init__(self, parent, target, htmldir):
		wx.html.HtmlWindow.__init__(self, parent)
		self.target = target
		self.htmldir = htmldir
	def setHtmlDir(self, htmldir):
		self.htmldir = htmldir
	def OnLinkClicked(self, link):
		if self.htmldir == None:
			self.target.SetPage("<html><body><h1>Configuration not complete</h1><p>htmldir not defined. Please supply a configuration file via the menu, or restart with a configuration file.</p></body></html>")
			self.target.Refresh(True)
			return
		href = link.GetHref()
		if href.startswith('unique:/'):
			(linksha256sum, linenumber) = href[8:].split('#')
			pphtml = bz2.BZ2File("%s/%s/%s/%s/%s.html.bz2" % (self.htmldir, linksha256sum[0], linksha256sum[1], linksha256sum[2], linksha256sum), 'r')
			htmlcontent = pphtml.read()
			pphtml.close()
			## grab the sha256sum of the files we found stuff in from the clicked link and display it
			self.target.SetPage(htmlcontent)
			self.target.ScrollToAnchor("line%s" % linenumber)
			self.target.Refresh(True)
		elif href.startswith("#"):
			anchor = href.split('#')[-1]
			self.ScrollToAnchor(anchor)


class BatFrame(wx.Frame):
	def __init__(self, parent, title, config):
		self.title = title
		wx.Frame.__init__(self, parent, title=self.title, size=(1280,768))
		self.origconfig = copy.copy(config)

		## some defaults
		self.datadir = ""
		self.tarfile = None
		self.timer = None
		self.selectedfile = None
		self.htmldir = None

		## we start in "simple" mode
		self.advanced = False
		self.advancedunpacked = False
		if sys.platform.startswith('linux'):
			if scanning:
				self.batconfig = ["Advanced mode", "Scanning mode"]
			else:
				self.batconfig = ["Advanced mode"]
		else:
			self.batconfig = ["Advanced mode"]
		self.batconfigstate = []

		## initial values of filters
		self.filterconfigstate = []
		self.filters = []
		self.filterconfig = [(["audio", "mp3", "ogg"], "Audio files"),
                                     (["emptydir"], "Empty directories (after filters have been applied)"),
                                     (["empty"], "Empty files"),
                                     (["png", "bmp", "jpg", "gif", "graphics"], "Graphics files"),
                                     (["pdf"], "PDF files"),
                                     (["resource"], "Resource files"),
                                     (["symlink"], "Symbolic links"),
                                     (["text", "xml"], "Text files"),
                                     (["video", "mp4"], "Video files"),]


		## add a few menus. This is heavily inspired by the tutorial code from wxPython
		menuBar = wx.MenuBar()

		filemenu = wx.Menu()
		menuOpen = filemenu.Append(wx.ID_OPEN,"&Open"," Open a scan archive")

		## Since scans are just on Linux there is no need to enable this option for anything else
		## right now.
		if sys.platform.startswith('linux'):
			if scanning:
				self.menuSaveAs = filemenu.Append(wx.ID_SAVEAS,"Save &As..."," Save results from a scan")
				if self.tarfile == None:
					self.menuSaveAs.Enable(False)

		filemenu.AppendSeparator()
		menuExit = filemenu.Append(wx.ID_EXIT,"E&xit"," Terminate the program")
		menuBar.Append(filemenu,"&File")

		configmenu = wx.Menu()
		menuConfigGeneral = configmenu.Append(wx.ID_ANY, "&General Configuration"," General configuration")
		menuConfigFilter = configmenu.Append(wx.ID_ANY, "&Filter Configuration"," Configure filters")
		configmenu.AppendSeparator()
		menuBar.Append(configmenu,"&Configuration")

		if sys.platform.startswith('linux'):
			self.batscanfile = ""
			menuScanOpenConfiguration = configmenu.Append(wx.ID_ANY,"Open &Configuration File"," Open a BAT configuration file")

			if scanning:
				menuConfigScans = configmenu.Append(wx.ID_ANY, "Configure &Scans"," Configure scans")
				scanmenu = wx.Menu()
				menuScanOpen = scanmenu.Append(wx.ID_ANY,"&Open file"," Open a file to scan")
				menuBar.Append(scanmenu,"&Scan")

		helpmenu = wx.Menu()
		#menuHelp = helpmenu.Append(wx.ID_HELP_CONTENTS, "&Contents"," Help")
		#menuAbout = helpmenu.Append(wx.ID_ABOUT, "&About"," Information about this program")
		#menuBar.Append(helpmenu,"&Help")

		self.SetMenuBar(menuBar)
		#self.Bind(wx.EVT_MENU, self.onAbout, menuAbout)
		self.Bind(wx.EVT_MENU, self.onExit, menuExit)
		self.Bind(wx.EVT_MENU, self.onOpen, menuOpen)
		#self.Bind(wx.EVT_MENU, self.onHelp, menuHelp)
		self.Bind(wx.EVT_MENU, self.onConfig, menuConfigGeneral)
		self.Bind(wx.EVT_MENU, self.onFilterConfig, menuConfigFilter)
		if sys.platform.startswith('linux') and scanning:
			self.Bind(wx.EVT_MENU, self.onSave, self.menuSaveAs)
			self.Bind(wx.EVT_MENU, self.onScanConfig, menuConfigScans)
			self.Bind(wx.EVT_MENU, self.onLaunchScan, menuScanOpen)
			self.Bind(wx.EVT_MENU, self.onScanOpenConfigFile, menuScanOpenConfiguration)

		## next we should have a toolbar
		battoolbar = self.CreateToolBar()

		## we create a sizer to position the elements
		## one row, two columns
		## column 0: file tree
		## column 1: display of all kinds of data
		## column 1 should be allowed to grow
		flexgrid = wx.FlexGridSizer(1, 2)
		flexgrid.AddGrowableCol(0, 3)
		flexgrid.AddGrowableCol(1, 15)
		flexgrid.AddGrowableRow(0)
		flexgrid.SetHGap(10)
		self.SetSizerAndFit(flexgrid)

		## Then we have two panels
		## The left panel is to show the file tree
		self.tree = wx.TreeCtrl(self, style=wx.TR_DEFAULT_STYLE|wx.TR_LINES_AT_ROOT|wx.TR_HAS_BUTTONS|wx.TR_FULL_ROW_HIGHLIGHT|wx.ALWAYS_SHOW_SB)
		flexgrid.Add(self.tree, flag=wx.EXPAND)
		self.tree.Bind(wx.EVT_TREE_SEL_CHANGED, self.treeHasChanged)
		
		## The right panel is to show all the other information. As soon as one of the items in the tree is selected
		## that is not a directory we change the information in all the panels.
		#notebookpanel = wx.Notebook(self)
		#self.notebookpanel = wx.aui.AuiNotebook(self)
		self.notebookpanel = wx.lib.agw.flatnotebook.FlatNotebook(self, agwStyle=wx.lib.agw.flatnotebook.FNB_NO_X_BUTTON|wx.lib.agw.flatnotebook.FNB_NO_NAV_BUTTONS)
		flexgrid.Add(self.notebookpanel, flag=wx.EXPAND)

		## There is an overview tab containing a window with some information, initially showing help text
		#overviewtab = wx.NotebookPage(notebookpanel)
		self.overviewwindow = wx.html.HtmlWindow(self.notebookpanel)
		self.notebookpanel.AddPage(self.overviewwindow, "Overview")
		self.overviewwindow.SetPage(helphtml)

		## We have a tab for detailed information about matches
		matchestab = wx.SplitterWindow(self.notebookpanel)
		self.notebookpanel.AddPage(matchestab, "String matches")
		## One window to display corresponding source code, initially empty
		self.matchesbrowser = wx.html.HtmlWindow(matchestab)
		self.matchesbrowser.SetPage("<html></html")
		## One window to display the matches, initially empty
		self.matcheswindow = BATHtmlWindow(matchestab, self.matchesbrowser, self.htmldir)
		self.matcheswindow.SetPage("<html></html>")
		matchestab.SplitVertically(self.matcheswindow, self.matchesbrowser)
		matchestab.SetSashGravity(0.5)

		## We have a tab for visual information and hexdumps about a file, only in advanced mode
		if self.advanced:
			self.initAlternateViewtab()

		'''
		## We have a tab for visual information and hexdumps about a file
		self.textpicturetab = wx.SplitterWindow(self.notebookpanel)
		self.textpicturetab.SetSashGravity(0.4)
		self.notebookpanel.AddPage(self.textpicturetab, "Text/pictures")
		if self.advanced:
			self.notebookpanel.EnableTab(3,False)
		else:
			self.notebookpanel.EnableTab(2,False)

		## with one textctrl on the right
		plaintextStyle = wx.TextAttr()
		plaintextStyle.SetFont(wx.FFont(10, family=wx.FONTFAMILY_TELETYPE))
		self.plaintextDisplay = wx.TextCtrl(self.textpicturetab, style=wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL)
		self.plaintextDisplay.SetDefaultStyle(plaintextStyle)

		## and pictures on the left
		self.picturedisplay = wx.ScrolledWindow(self.textpicturetab)
		displayvbox = wx.FlexGridSizer(2, 1)
		displayvbox.AddGrowableRow(0,1)
		displayvbox.AddGrowableRow(1,1)
		displayvbox.SetVGap(5)
		self.picturedisplay.SetSizerAndFit(displayvbox)

		self.textpicturetab.SplitVertically(self.picturedisplay, self.plaintextDisplay)
		self.textpicturetab.SetSashGravity(0.4)
		'''

		self.initConfig(self.origconfig)

		self.Show(True)

	def initConfig(self, config):
		self.scanconfig = []
		for s in config.sections():
			if s == 'batconfig':
				continue
			elif s == 'viewer':
				if config.has_option(s, 'htmldir'):
					self.htmldir = config.get(s, 'htmldir')
					self.matcheswindow.setHtmlDir(self.htmldir)
			else:
				try:
					## process each section. We need: section name, description, enabled
					description = config.get(s, 'description')
					enabled = config.get(s, 'enabled')
					self.scanconfig.append((s, description, enabled))
				except:
					pass
		self.scanconfigstate = []
		for s in self.scanconfig:
			if s[2] == 'yes':
				self.scanconfigstate.append(self.scanconfig.index(s))

	def initAlternateViewtab(self):
		self.alternateviewtab = wx.SplitterWindow(self.notebookpanel)
		self.notebookpanel.InsertPage(2, self.alternateviewtab, "Alternate view", select=False)

		## with one textctrl on the right
		hexdumpStyle = wx.TextAttr()
		hexdumpStyle.SetFont(wx.FFont(10, family=wx.FONTFAMILY_TELETYPE))
		self.textCtrl = wx.TextCtrl(self.alternateviewtab, style=wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL)
		self.textCtrl.SetDefaultStyle(hexdumpStyle)

		## and pictures on the left
		self.picturepanel = wx.ScrolledWindow(self.alternateviewtab)
		vbox = wx.FlexGridSizer(2, 1)
		vbox.AddGrowableRow(0,1)
		vbox.AddGrowableRow(1,1)
		vbox.SetVGap(5)
		self.picturepanel.SetSizerAndFit(vbox)

		self.histo2 = wx.EmptyBitmap(width=1, height=1)
		self.histogram2 = wx.lib.statbmp.GenStaticBitmap(self.picturepanel, ID=-1, bitmap=self.histo2)
		#self.histogram = wx.StaticBitmap(self.picturepanel, bitmap=histo2)
		#self.histogram2 = wx.lib.statbmp.GenStaticBitmap(self.picturepanel, ID=-1, bitmap=histo2)
		#self.histogram2.Bind(wx.EVT_LEFT_DOWN, self.onHexdumpClick)
		vbox.Add(self.histogram2, flag=wx.EXPAND)
		#vbox.Add(self.histogram, flag=wx.EXPAND)
		self.picturepanel.SetScrollbars(20, 20, 10, 10)
		self.alternateviewtab.SplitVertically(self.picturepanel, self.textCtrl)
		self.alternateviewtab.SetSashGravity(0.4)

	def onHexdumpClick(self, event):
		## get a rough approximation of where to scroll to in the hexdump file
		(xpos,ypos) = event.GetPositionTuple()
		pos = ((xpos + ypos * 512)/16) * 79
		self.textCtrl.SetInsertionPoint(pos)

	def textctrlupdate(self):
		## in case the file had already been changed, and CallLater() had not yet run we would
		## get an error. It is actually better to make sure CallLater() is aborted on time
		if self.data == '':
			return
		offset = 1000000
		self.textCtrl.Freeze()
		self.textCtrl.AppendText(self.data[offset:])

		unpackedfiles = []
		for i in self.unpackreports[self.selectedfile]['scans']:
			unpackedfiles.append((i['offset'], i['scanname'], i['size']))

		## work our way backwards, so we don't have to remember to do funky math with offsets
		unpackedfiles = sorted(unpackedfiles, reverse=True)
		for i in unpackedfiles:
			if len(self.data) < 78:
				continue
			pos = (i[0]/16) * 79 + 78
			labelStyle = wx.TextAttr()
			labelStyle.SetFlags(wx.TEXT_ATTR_TEXT_COLOUR)
			labelStyle.SetTextColour('RED')
			self.textCtrl.SetInsertionPoint(pos)
			insertionText = " %s at 0x%08x" % (i[1], i[0])
			self.textCtrl.WriteText(insertionText)
			self.textCtrl.SetStyle(pos, pos + len(insertionText), style=labelStyle)
		self.textCtrl.SetInsertionPoint(0)
		self.textCtrl.Thaw()

	def treeHasChanged(self, event):
		selected = self.tree.GetItemData(event.GetItem()).GetData()
		if selected == self.selectedfile:
			return
		self.selectedfile = selected

		## if there are any times, stop and destroy them
		if self.timer != None:
			self.timer.Stop()
		## first clean all the windows
		self.cleanWindows()
		if self.advanced:
			## clean the pictures
			self.histo2 = wx.EmptyBitmap(width=1, height=1)
			self.histogram2.SetBitmap(bitmap=self.histo2)
			self.histogram2.Refresh(True)
			self.picturepanel.FitInside()
			## clean the hexdump
			self.textCtrl.Clear()
			## enable tabs again
			self.notebookpanel.EnableTab(2,True)

		#self.notebookpanel.EnableTab(1,True)

		## if / is selected we print the help page again
		if self.selectedfile == None:
			self.overviewwindow.SetPage(helphtml)

		if self.unpackreports.has_key(self.selectedfile):
			sha256sum = ''
			if self.unpackreports[self.selectedfile].has_key('sha256'):
				sha256sum = self.unpackreports[self.selectedfile]['sha256']
			## now we have to change all the tabs. First the overview
			name = self.unpackreports[self.selectedfile]['name']
			path = self.unpackreports[self.selectedfile]['path']
			realpath = self.unpackreports[self.selectedfile]['realpath']
			magic = self.unpackreports[self.selectedfile]['magic']
			if not "symbolic link" in magic:
				if self.unpackreports[self.selectedfile].has_key('size'):
					size = self.unpackreports[self.selectedfile]['size']
				else:
					size = 0
			else:
				size = 0
			## very simplistic HTML table we show for individual elements
			## All in 1990s HTML!
			overviewstring = '''
<html>
<body>
<h2>Overview</h2>
<table>
<tr><td><b>Name</b></td><td>%s</td></tr>
<tr><td><b>Path</b></td><td>%s</td></tr>
<tr><td><b>Absolute path</b></td><td>%s</td></tr>
<tr><td><b>Size</b></td><td>%s bytes</td></tr>
<tr><td><b>File type</b></td><td>%s</td></tr>
			'''
			footer = '''
</body>
</html>
			'''
			tag = ""
			tablerows = ''
			matchesrows = ''
			functionmatches = ''
			tablerowtemplate = "<tr><td><b>%s</b></td><td>%s</td></tr>\n"
			if sha256sum != '':
				tablerows = tablerows + tablerowtemplate % ("SHA256", sha256sum)
			if self.leafreports.has_key(self.selectedfile):
				applications = []
				i = self.leafreports[self.selectedfile]
				if i.has_key('busybox-version'):
					tablerows = tablerows + tablerowtemplate % ("BusyBox version", i['busybox-version'])
				if i.has_key('architecture'):
					tablerows = tablerows + tablerowtemplate % ("Architecture", i['architecture'])
				if i.has_key('kernelmodulelicense'):
					tablerows = tablerows + tablerowtemplate % ("Kernel module license", i['kernelmodulelicense'])
				if i.has_key('libs'):
					tablerows = tablerows + tablerowtemplate % ("Shared libraries", reduce(lambda x, y: "%s, %s" % (x,y), i['libs']))
				if i.has_key('licenses'):
					tablerows = tablerows + tablerowtemplate % ("Licenses/license families", reduce(lambda x, y: "%s, %s" % (x,y), i['licenses'].keys()))
				if i.has_key('forges'):
					tablerows = tablerows + tablerowtemplate % ("Forges", reduce(lambda x, y: "%s, %s" % (x,y), i['forges'].keys()))
				if i.has_key('redboot'):
					tablerows = tablerows + tablerowtemplate % ("Bootloader", "RedBoot")
				for j in ['dproxy', 'ez-ipupdate', 'iproute', 'iptables', 'libusb', 'loadlin', 'vsftpd', 'wireless-tools', 'wpa-supplicant']:
					if i.has_key(j):
						applications.append(j)
				if i.has_key('kernelchecks'):
					for j in i['kernelchecks']:
						if j == 'version':
							tablerows = tablerows + tablerowtemplate % ("Linux kernel", i['kernelchecks']['version'])
				if i.has_key('tags'):
					if i['tags'] != []:
						tablerows = tablerows + tablerowtemplate % ("Tags", reduce(lambda x, y: x + ", " + y, i['tags']))
					if 'graphics' in i['tags']:
						if self.advanced:
							self.notebookpanel.EnableTab(2,False)
						tag = 'graphics'
					if 'text' in i['tags']:
						if self.advanced:
							self.notebookpanel.EnableTab(2,False)
						tag = 'text'
				if i.has_key('ranking'):
					(stringsres, dynamicres) = i['ranking']
					if dynamicres.has_key('packages'):
						functionmatches = '''<h2>Function match statistics</h2>
<table>
<tr><td><b>Extracted function names</b></td><td>%d</td></tr>
<tr><td><b>Matched function names</b></td><td>%d</td></tr>
</table>
'''
						functionmatches = functionmatches % (dynamicres['totalnames'], (dynamicres['namesmatched']))
						functionmatches = functionmatches + '''<h3>Matched packages (function names method)</h3>
<table><tr><td>
  <table>
    <tr><td><b>Name</b></td><td><b>Unique matches</b></td></tr>\n'''
						for j in dynamicres['packages']:
								## for now: just take the version with the most matches, only report the amount of matches
								functionmatches = functionmatches + "    <tr><td>%s</td><td>%d</td></tr>\n" % (j, max(map(lambda x: x[1], dynamicres['packages'][j])))
						functionmatches = functionmatches + "</table>"

					if not (stringsres['extractedlines']) == 0 and not (stringsres['matchedlines']) == 0:
						matchesrows = '''
<h2>String match statistics</h2>
<table>
<tr><td><b>Extracted lines</b></td><td>%d</td></tr>
<tr><td><b>Matched lines</b></td><td>%d</td></tr>
<tr><td><b>Match percentage</b></td><td>%f%%</td></tr>
</table>
						'''
						matchesrows = matchesrows % (stringsres['extractedlines'], stringsres['matchedlines'], (float(stringsres['matchedlines'])/stringsres['extractedlines']*100))
						if len(stringsres['reports']) != 0:
							htmllinks = []
							versionhtml = ""
							## nested table, urgghhhh
							matchesrows = matchesrows + '''<h3>Matched packages (strings method)</h3>
<table>
  <tr><td>
    <table>
      <tr>
        <td><b>Rank</b></td>
        <td><b>Name</b></td>
        <td><b>Percentage</b></td>
        <td><b>Unique matches</b></td>
        <td><b>Non unique matches assigned</b></td>
        <td><b>Possible licenses</b></td>
      </tr>\n'''

							for j in stringsres['reports']:
								if len(j[5]) > 0:
									if len(j[5]) > 1:
										licensestring = j[5][0] + reduce(lambda x, y: x + " " + y, j[5][1:], "")
									else:
										licensestring = j[5][0]
								else:
									licensestring = ""
								matchesrows = matchesrows + "    <tr><td>%d</td><td>%s</td><td>%f%%</td><td>%d</td><td>%d</td><td>%s</td></tr>\n" % (j[0], j[1], j[3], j[2], stringsres['nonUniqueAssignments'].get(j[1], 0), licensestring)
								if j[2] != 0:
									## don't replace %s/% with os.path.join here, since this is valid in HTML
									versionhtml = versionhtml + "<h5>%s</h5><p><img src=\"%s\"/></p>\n" % (j[1], "%s/%s-%s-version.png" % (self.imagesdir, sha256sum, j[1]))
									## here we should either do a database lookup to get the checksum,
									## or check if they are already in the report
									htmllinks.append((j[1], j[2]))
							## don't replace %s/% with os.path.join here, since this is valid in HTML
							matchesrows = matchesrows + "</table><td><img src=\"%s\"/></td></tr></table>" % ("%s/%s-piechart.png" % (self.imagesdir, sha256sum))
							if versionhtml != "":
								matchesrows = matchesrows + "<h2>Versions per package</h2>" + versionhtml
							if os.path.exists(os.path.join(self.reportsdir, "%s-unique.html.gz" % sha256sum)):
								uniquehtmlfile = gzip.open(os.path.join(self.reportsdir, "%s-unique.html.gz" % sha256sum), 'r')
								uniquehtml = uniquehtmlfile.read()
								uniquehtmlfile.close()
								self.matcheswindow.SetPage(uniquehtml)
				## TOT HIER
				if applications != []:
					tablerows = tablerows + "<tr><td><b>Applications</b></td><td>%s</td></tr>\n" % reduce(lambda x, y: "%s, %s" % (x,y), applications)
			overviewstring = overviewstring % (name, path, realpath, size, magic)
			overviewstring = overviewstring + tablerows + "</table>" + functionmatches + matchesrows + footer
			self.overviewwindow.SetPage(overviewstring)

			if sha256sum != '' and tag not in ['graphics', 'text', 'compressed', 'audio', 'video', 'resource']:
				if self.advanced:
					try:
						os.stat(os.path.join(self.reportsdir, "%s-hexdump.gz" % (sha256sum,)))
						datafile = gzip.open(os.path.join(self.reportsdir, '%s-hexdump.gz' % (sha256sum,)))
						hexdump = datafile.read()
						datafile.close()
						self.data = hexdump
						self.datalen = len(self.data)
						## if file is small enough load it at once
						if size > 1000000:
							self.textCtrl.WriteText(self.data[:1000000])
							self.timer = wx.CallLater(2000, self.textctrlupdate)
						else:
							self.textCtrl.WriteText(self.data)
							self.timer = wx.CallLater(0, self.textctrlupdate)
						self.textCtrl.SetInsertionPoint(0)
					except Exception, e:
						pass
					try:
						os.stat(os.path.join(self.imagesdir, "%s.png" % (sha256sum,)))
						self.histo2 = wx.Image(os.path.join(self.imagesdir, '%s.png' % (sha256sum,)), wx.BITMAP_TYPE_ANY).ConvertToBitmap()
		
						self.histogram2.SetBitmap(bitmap=self.histo2)
						self.histogram2.Bind(wx.EVT_LEFT_DOWN, self.onHexdumpClick)
						self.histogram2.Refresh(True)
						self.picturepanel.FitInside()
					except Exception, e:
						pass

	def initTree(self, tree, tmpdir):
		if tmpdir == None:
			return
		tree.DeleteAllItems()
		self.cleanWindows()

		## construct a tree from a pickle
		if tmpdir.endswith('/'):
			tmpdir = tmpdir[:-1]

		rootnode = tree.AddRoot('/')
		nodes = {}

		dirlist = list(set(map(lambda x: os.path.dirname(x), self.unpackreports.keys())))

		## make sure that we have all directories
		for d in dirlist:
			if os.path.dirname(d) in dirlist:
				continue
			else:
				dirlist.append(os.path.dirname(d))
		dirlist.sort()

		for d in dirlist:
			if d == "":
				continue
			else:
				if d.startswith('/'):
					d = d[1:]
				parent = os.path.dirname(d)
				if parent == "":
					linktext = u"%s  \u24b9" % d
					dirnode = tree.AppendItem(rootnode, linktext, data=wx.TreeItemData(d))
					nodes[d] = dirnode
				else:
					## length of parent, plus 1 for trailing slash
					parentlen = len(parent) + 1

					## check if the parent directory is actually there. If not, we have
					## a problem. Should not occur.
					if not nodes.has_key(parent):
						continue
					#linktext = u"%s \u1f4c1" % j
					linktext = u"%s  \u24b9" % d[parentlen:]
					dirnode = tree.AppendItem(nodes[parent], linktext, data=wx.TreeItemData(os.path.normpath(d)))
					nodes[d] = dirnode

		filelist = self.unpackreports.keys()
		filelist.sort()

		for j in filelist:
			if j.startswith('/'):
				j = j[1:]
			parent = os.path.dirname(j)
			## length of parent, plus 1 for trailing slash
			parentlen = len(parent) + 1
			ignore = False
			if parent == "":
				linktext = j
			else:
				linktext = j[parentlen:]
			if self.leafreports.has_key(j):
				for r in self.leafreports[j]:
					if r == 'ranking':
						if self.leafreports[j]['ranking'][0]['matchedlines'] != 0:
							## give a visual clue to people to tell them that strings could be matched
							#linktext = u"%s  \u24e7" % linktext
							linktext = u"%s  \u272a" % linktext
					tagsentities = {'text': u'\u24c9', 'graphics': u'\u24bc', 'compressed': u'\u24b8', 'resource': u'\u24c7', 'dalvik': u'\u24b6'}
					if r == 'tags':
						if list(set(self.leafreports[j]['tags']).intersection(set(self.filters))) != []:
							ignore = True
							continue
						tagappend = u""
						for t in self.leafreports[j]['tags']:
							if tagsentities.has_key(t):
								tagappend += tagsentities[t]
						if tagappend != u"":
							linktext = linktext + u"  %s" % tagappend
			elif "symbolic link" in self.unpackreports[j]['magic']:
				if "symlink" in self.filters:
					ignore = True
					continue
				## if we have a link, then add the value of where the link points to
				## to give a visual clue to people
				## example: "symbolic link to `../../bin/busybox'"
				linkname = self.unpackreports[j]['magic'][:-1].rsplit("symbolic link to `", 1)[-1]
				linktext = u"%s \u2192 %s" % (linktext, linkname)
			elif self.unpackreports[j].has_key('size'):
				if self.unpackreports[j]['size'] == 0:
					## if files are empty we mark them as empty
					if "empty" in self.filters:
						ignore = True
					else:
						linktext = u"%s  \u2205" % linktext
			if ignore:
				continue
			if parent == "":
				leafnode = tree.AppendItem(rootnode, linktext, data=wx.TreeItemData(j))
				nodes[j] = leafnode
			else:
				if not nodes.has_key(parent):
					continue
				else:
					leafnode = tree.AppendItem(nodes[parent], text=linktext, data=wx.TreeItemData(os.path.normpath(j)))
					nodes[j] = leafnode

		## Remove empty, or seemingly empty, directories from the
		## view to declutter the interface.
		## We keep traversing the tree until we know for sure that
		## there are only directories with visible items left. There
		## is probably a more efficient way, but this is still fast.
		if "emptydir" in self.filters:
			stillempty = True
			while stillempty:
				stillempty = False
				for i in dirlist:
					if nodes.has_key(i):
						if not tree.ItemHasChildren(nodes[i]):
							tree.Delete(nodes[i])
							del nodes[i]
							stillempty = True
		tree.SortChildren(rootnode)
		for n in nodes:
			tree.SortChildren(nodes[n])
		tree.Refresh()
		tree.ExpandAll()

		if self.selectedfile != None:
			## reselect the already selected item and scroll to the right place
			if nodes.has_key(self.selectedfile):
				tree.SelectItem(nodes[self.selectedfile])
				tree.ScrollTo(nodes[self.selectedfile])
			else:
				## TODO: refactor this
				## first clean all the windows
				self.cleanWindows()
				if self.advanced:
					## clean the pictures
					self.histo2 = wx.EmptyBitmap(width=1, height=1)
					self.histogram2.SetBitmap(bitmap=self.histo2)
					self.histogram2.Refresh(True)
					self.picturepanel.FitInside()
					## clean the hexdump
					self.textCtrl.Clear()

				self.selectedfile = None

	def cleanWindows(self):
		self.overviewwindow.SetPage(helphtml)
		self.matcheswindow.SetPage('<html></html>')
		self.matchesbrowser.SetPage("<html></html>")

	## based on configuration we enable/disable certain features
	def redrawGUI(self):
		if not 0 in self.batconfigstate and self.advanced == True:
			self.advanced = False
			self.notebookpanel.RemovePage(2)
			self.SetTitle(self.title)
		if 0 in self.batconfigstate and self.advanced == False:
			self.advanced = True
			if self.advancedunpacked == False and self.tarfile != None:
				try:
					tar = tarfile.open(self.tarfile)
					members = []
					## only unpack certain files and directories
					members = members + filter(lambda x: x.name.startswith('images') and len(os.path.basename(x.name)) == 68, self.tarmembers)
					members = members + filter(lambda x: x.name.startswith('reports') and x.name.endswith('-hexdump.gz'), self.tarmembers)
					tar.extractall(self.tmpdir, members)
					tar.close()
					self.advancedunpacked = True
				except Exception, e:
					pass
			self.initAlternateViewtab()
			self.SetTitle(self.title + " (advanced mode)")
		self.Refresh()

	## show a dialog window with some information about BAT
	def onAbout(self,e):
		# A message dialog box with an OK button. wx.OK is a standard ID in wxWidgets.
		dlg = wx.MessageDialog( self, "Binary Analysis Tool Viewer", "About the Binary Analysis Tool Viewer", wx.OK)
		dlg.ShowModal()
		dlg.Destroy()


	def onFilterConfig(self,e):
		dlg = wx.MultiChoiceDialog(self, "Choose file types that should be ignored in the file tree.\nChosen file types will NOT be displayed.", "Binary Analysis Tool file type filter", map(lambda x: x[1], self.filterconfig))
		dlg.SetSelections(self.filterconfigstate)
		if dlg.ShowModal() == wx.ID_OK:
			self.filterconfigstate = dlg.GetSelections()
			self.filters = reduce(lambda x, y: x + y, map(lambda x: self.filterconfig[x][0], self.filterconfigstate), [])
			if self.datadir != "":
				self.initTree(self.tree, self.datadir)
		dlg.Destroy()

	def onConfig(self,e):
		dlg = wx.MultiChoiceDialog(self, "Binary Analysis Tool configurator", "", self.batconfig)
		dlg.SetSelections(self.batconfigstate)
		if dlg.ShowModal() == wx.ID_OK:
			self.batconfigstate = dlg.GetSelections()
			self.redrawGUI()
		dlg.Destroy()

	def onScanConfig(self,e):
		dlg = wx.MultiChoiceDialog(self, "Binary Analysis Tool scan configurator", "", map(lambda x: x[1], self.scanconfig))
		## we don't get a maximize_box
		## see http://stackoverflow.com/questions/8579189/wxwidgets-dialog-doesnt-get-a-maximize-box
		#dlg.SetExtraStyle(wx.MAXIMIZE_BOX)
		#dlg.Refresh()
		dlg.SetSelections(self.scanconfigstate)
		if dlg.ShowModal() == wx.ID_OK:
			self.scanconfigstate = dlg.GetSelections()
		dlg.Destroy()

	def onScanOpenConfigFile(self,e):
		dlg = wx.FileDialog(self, 'Open BAT configuration file', style=wx.FD_OPEN|wx.FD_FILE_MUST_EXIST)
		if dlg.ShowModal() == wx.ID_OK:
			config = ConfigParser.ConfigParser()
			try:
				configfile = open(dlg.GetPath(), 'r')
				config.readfp(configfile)
				self.origconfig = copy.copy(config)
				self.initConfig(self.origconfig)
			except Exception, e:
				errordiag = wx.MessageDialog(None, 'Not a valid configuration file', 'Error', wx.OK | wx.ICON_ERROR)
				errordiag.ShowModal()
		dlg.Destroy()

	def onLaunchScan(self,e):
		dlg = wx.FileDialog(self, 'Open BAT results', style=wx.FD_OPEN|wx.FD_FILE_MUST_EXIST)
		if dlg.ShowModal() == wx.ID_OK:
			## create a new temporary directory
			if self.scanconfig != []:
				self.tmpdir = tempfile.mkdtemp()
				config = ConfigParser.ConfigParser()
				enabled_configs = []
				for i in self.scanconfigstate:
					enabled_configs.append(self.scanconfig[i][0])
				enabled_configs.append('batconfig')
				enabled_configs.append('viewer')

				for c in self.origconfig.sections():
					if c in enabled_configs:
						config.add_section(c)
						for i in self.origconfig.items(c):
							config.set(c, i[0], i[1])
				self.scans = bat.bruteforcescan.readconfig(config)
				(self.unpackreports, self.leafreports) = bat.bruteforcescan.runscan(self.tmpdir, self.scans, dlg.GetPath())
				bat.bruteforcescan.dumpData(self.unpackreports, self.leafreports, self.scans, self.tmpdir)

				## After running the scan we dump the results, repopulate the tree and
				## work from there.
				self.datadir = os.path.join(self.tmpdir, "data")
				self.imagesdir = os.path.join(self.tmpdir, "images")
				self.reportsdir = os.path.join(self.tmpdir, "reports")
				self.initTree(self.tree, self.datadir)
				self.menuSaveAs.Enable(True)
			else:
				errordiag = wx.MessageDialog(None, 'No configuration defined', 'Error', wx.OK | wx.ICON_ERROR)
				errordiag.ShowModal()
		dlg.Destroy()

	def onExit(self,e):
		self.Close(True)

	## TODO: create help
	def onHelp(self,e):
		print e.GetString()
		return

	def onSave(self,e):
		dlg = wx.FileDialog(self, 'Save BAT results', style=wx.FD_SAVE|wx.FD_OVERWRITE_PROMPT)
		if dlg.ShowModal() == wx.ID_OK:
			self.tarfile = dlg.GetPath()
			bat.bruteforcescan.writeDumpfile(self.unpackreports, self.leafreports, self.scans, self.tarfile, self.tmpdir)

	def onOpen(self,e):
		dlg = wx.FileDialog(self, 'Open BAT results', style=wx.FD_OPEN|wx.FD_FILE_MUST_EXIST)
		if dlg.ShowModal() == wx.ID_OK:
			## should be an archive with inside:
			## * scandata.pickle
			## * data directory
			## * images directory (optional)
			self.tmpdir = tempfile.mkdtemp()
			try:
				self.tarfile = dlg.GetPath()
				tar = tarfile.open(self.tarfile)
				self.tarmembers = tar.getmembers()
				members = []
				for i in ['scandata.pickle']:
					members = members + filter(lambda x: x.name.startswith(i), self.tarmembers)
				## If we are not in advanced mode, there is no need to unpack everything. The hexdump
				## files and "TV static" pictures can be quite big, so don't unpack them when not needed.
				if not self.advanced:
					members = members + filter(lambda x: x.name.startswith('reports') and x.name.endswith('unique.html.gz'), self.tarmembers)
					members = members + filter(lambda x: x.name.startswith('images') and len(os.path.basename(x.name)) != 68, self.tarmembers)
				else:
					members = members + filter(lambda x: x.name.startswith('reports'), self.tarmembers)
					members = members + filter(lambda x: x.name.startswith('images'), self.tarmembers)
					self.advancedunpacked = True
				tar.extractall(self.tmpdir, members)
				tar.close()
			except Exception, e:
				os.rmdir(self.tmpdir)
				return
			self.datadir = os.path.join(self.tmpdir, "data")
			self.imagesdir = os.path.join(self.tmpdir, "images")
			self.reportsdir = os.path.join(self.tmpdir, "reports")
			picklefile = open(os.path.join(self.tmpdir, "scandata.pickle") , 'rb')
			(self.unpackreports, self.leafreports, scans) = cPickle.load(picklefile)
			picklefile.close()
			self.selectedfile = None
			self.initTree(self.tree, self.datadir)
			if sys.platform.startswith('linux') and scanning:
				self.menuSaveAs.Enable(False)


def main(argv):
	config = ConfigParser.ConfigParser()
	parser = OptionParser()
	parser.add_option("-c", "--config", action="store", dest="cfg", help="path to configuration file", metavar="FILE")
	(options, args) = parser.parse_args()

	if options.cfg != None:
		try:
			configfile = open(options.cfg, 'r')
			config.readfp(configfile)
		except:
			print >>sys.stderr, "Need configuration file"
			sys.exit(1)

	app = wx.App(False)  # Create a new app, don't redirect stdout/stderr to a window.
	frame = BatFrame(None, "Binary Analysis Tool", config) # A Frame is a top-level window.
	frame.Show(True)     # Show the frame.
	app.MainLoop()

if __name__ == "__main__":
	main(sys.argv)
