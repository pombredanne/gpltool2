TODO:

file systems:
* FAT16 (use python-construct)
* FAT32
* MBR (use python-construct)
* NTFS
* ubifs (unubi was removed from mtd-utils)
* more generic YAFFS2 support
* squashfs as described at http://www.devttys0.com/2011/08/extracting-non-standard-squashfs-images/
* ext4 using extents

compression/executables:
* E00
* 7z only works if a whole file is compressed with 7z. Rework and test with D-Link firmware DSL-524T_FW_V3.00B01T02.UK-A.20060621.zip
* better support for ZIP file comments http://www.pkware.com/documents/casestudies/APPNOTE.TXT
* use python-pefile for PE analysis
* NBG5615 uImage
* ncompress on Ubuntu gives false positives: try to filter these out

dynamic ELF scanning
* take RPATH into account
* add more functions from LSB and other standards

queueing system:
* let top level script also read from a queue
* add script to add files to and remove files from scanning queue

GUI:
* rewrite to PyQt or PySide to take advantage of better rendering engine (webkit)
* add reports to GUI for distribution checks
* move detailed function name reports from overview to function name report
* rework reporting of duplicate files
* guireports.py: report all names for a certain checksum (for example if there are copies of a file under a different name)

error handling:
* better handle errors to give users a better indication if something went wrong

database creation:
* rename batchextractprogramstrings.py
* replace excessive parameter list of batchextractprogramstrings.py with a configuration file
* handle embedded archives (ZIP files, tarballs, etc.)
* handle patches (both normal and unified)
* import licensing information from SPDX files
* rewrite ctags and xgettext invocations to scan multiple files at once to reduce overhead of calling ctags and xgettext
* extract more information from Linux kernel, including values from __ATTR and friends, as far as possible
* extract name, type and description from kernel for module descriptions (module_param and friends) and store for further kernel module analysis, plus also for kernel image detection (sometimes parameters are recorded in the form 'module.param')

ranking:
* use version information to report most likely licenses
* use macholib to analyse Mach-O binaries (MacOS X)
* split extractGeneric into two methods: one to extract all the info (including kernel module names and kernel function names), one for computing the end result
* if there are more results for file for a line (different line numbers), combine the two results and put the line numbers in a list (first do research to see if this makes sense)

busybox scanning:
* clean up finding ranges of applets in extract_configuration. It should be possible to do this less kludgy.

HTML generation:
* finish function name reporting (Java, Linux kernel)
* rework variable name reporting
* clean up/rewrite/make it easier to use
* add license information if available

image generation:
* make cut-off values for pie chart generation configurable (but default to current values if not defined)

misc:
* replay script to unpack firmware based on only the pickle with unpacking data. After this set the default of outputlite to 'yes'
* add configuration option to set temporary directory prefix for all scans
* add piecharts/reports per directory that summarise what is inside a directory (recursively, perhaps only if something was unpacked)
* add per scan debug to allow better custom debugging
* test on latest OpenSUSE
* test on latest Ubuntu, make sure configurations/dependencies work
* test on latest Debian, make sure configurations/dependencies work
* replace calls to dd, tail, head and others with truncate() where possible to prevent launching an external process
* replace hardcoded options in reporting with values of 'storetarget'
* add fonttools dep to Debian/Ubuntu dependencies
* add documentation about rationale of current ordering of scans (especially unpacking scans), plus of default 'noscan' values
* rename "programscans"/"leafscans" and settle on using one term

BOTTLENECKS:
1. ELF tagging is sometimes incorrect, so LZMA unpacker tries to extract LZMA files in vain from these ELF files, which costs a lot of time.
2. preread (in parallel) leafreports of Java .class files which are shared between several JAR files for aggregate jars. This saves opening and reading the same files.
3. replace own counters with collections.Counter(), also for code clarity
4. generate pie charts before determining version information to prevent huge pickles being read into memory. Do the same for unmatched results (HTML report).
5. verify and tag Android resources.arsc
6. rework datastructures of ranking information (strings, function names, variables) so they are all the same
7. rework ranking as an aggregate scan to be able to do database lookups in parallel for strings per file instead of per file.
8. rework ELF graphs (allow edges to cross) as the images can get very very big for certain firmwares
9. add ramdisk unpacking for jffs2
10. research/fix priority for Minix file system
11. don't recreate ELF graphs (model for every ELF binary), but reuse graphs of dependencies. This would need a topological sort.
12. get rid of the 'templink' hacks in ISO, TAR and PDF unpacking, because of possible race conditions
