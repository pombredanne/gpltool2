\documentclass[11pt]{beamer}

\usepackage{url}
\usepackage{tikz}
%\author{Armijn Hemel}
\title{Using the Binary Analysis Tool - part 3}
\date{}

\begin{document}

\setlength{\parskip}{4pt}

\frame{\titlepage}

\frame{
\frametitle{Subjects}
In this course you will learn:

\begin{itemize}
\item about the scan order in the Binary Analysis Tool
\item to configure the Binary Analysis Tool
\end{itemize}
}

\frame{
\frametitle{Scan order in the Binary Analsysis Tool}

\begin{enumerate}
\item the binary is read and file type specific identifiers (from a hardcoded list) are searched for in the binary
\item prerun scans are run to tag files to filter out specific file types later
\item unpacking scans are run to unpack any compressed files or file systems and scans 1 - 3 are run recursively
\item file specific scans are run on all unpacked files
\item post run scans are run for each individual file
\end{enumerate}
}

\frame{
\frametitle{Identifier search}
The identifier search uses a hardcoded list of identifiers that indicate where a certain file starts or stops. Not all file types have identifiers, but most do.

The locations of identifiers are passed to later scans, which can use this information to work in a more efficient way.
}

\frame{
\frametitle{Prerun scans}
Prerun scans are run to determine the type of the \textit{entire} file and to tag it. Tags can be used by later scans to ignore files: a scan to unpack a file system won't work on a graphics file or a text file.

Some tags that can be set by prerun scans in the current version of BAT are:

\begin{itemize}
\item text
\item binary
\item graphics
\item audio
\item elf
\item compressed
\end{itemize}

plus more to come.
}

\frame{
\frametitle{Unpacking scans}
Unpacking scans try to extract file systems and compressed files, sometimes from a larger binary blob, by ``carving'' it from a larger binary. Currently some 30 file systems and compressed files are supported:

\begin{itemize}
\item file systems: cramfs, ext2/ext3/ext4, ISO9660, JFFS2, Minix (specific
variant of v1), SquashFS (several variants), romfs, YAFFS2 (specific variants)
\item compressed files and executable formats: 7z, ar, ARJ, BASE64, BZIP2,
compressed Flash, CAB, compress, CPIO, EXE (specific compression methods only),
GZIP, InstallShield (old versions), LRZIP, LZIP, LZMA, LZO, RAR, RPM, serialized
Java, TAR, UPX, XZ, ZIP
\item media files: GIF, ICO, PDF, PNG
\end{itemize}
}

\frame{
\frametitle{File specific scans}

\begin{itemize}
\item dynamically linked libraries
\item quick scan for identifiers of several programs (iptables, wireless-tools, etcetera)
\item quick scan for identifiers of several licenses
\item quick scan for identifiers of several collaborative software development sites (sourceforge, github, etcetera)
\item Linux kernel version check
\item BusyBox version check
\item advanced string matching scan using information from a large database extracted from source code
\end{itemize}
}

\frame{
\frametitle{Postrun scans}
Postrun scans are scans that don't modify the files (unpacking, or scanning files), but merely act on results of scans, for example:

\begin{itemize}
\item generating reports of the results
\item generating pictures of the results
\end{itemize}
}

\frame{
\frametitle{Configuring the Binary Analysis Tool}

The Binary Analysis Tool is highly configurable and uses plugins. These plugins can be enabled and disabled via a configuration file.

The configuration file is in Windows INI format and contains several parts:

\begin{itemize}
\item general configuration
\item configuration directives for ``prerun'' scans
\item configuration directives for ``unpack'' scans
\item configuration directives for per file scans
\item configuration directives for ``postrun'' scans
\end{itemize}

The general configuration is mandatory, the other directives are optional.
}

\begin{frame}[fragile]
\frametitle{General configuration}

\begin{verbatim}
[batconfig]
multiprocessing = no
module = bat.simpleprettyprint
output = prettyprintresxml
\end{verbatim}

The \texttt{multiprocessing} option is to enable the use of multiple processors. Usually this is safe, unless one of the scans is not safe (for example because it writes to a database).

The \texttt{output} and \texttt{module} options are used together to specify where an (optional) method that prints results in XML can be found.

There is an optional configuration \texttt{outputlite} that, if set, tells BAT to omit a full copy of the data from the output archive.

\end{frame}

\frame{
\frametitle{Mandatory scan configuration options}
The configuration for each type of scan has a few mandatory options:

\begin{itemize}
\item \texttt{type} - \texttt{prerun}, \texttt{unpack}, \texttt{program} or \texttt{postrun}
\item \texttt{module} - Python module (including package) the scan can be found
\item \texttt{method} - the method for the scan
\item \texttt{enabled} - \texttt{yes} enables a scan, \texttt{no} disables a scan
\end{itemize}
}

\frame{
\frametitle{Optional scan configuration options}

\begin{itemize}
\item \texttt{priority} - scan priority: a higher priority means it is run before scans with a lower priority.
\item \texttt{noscan} - list of tags. Files that are tagged with any of these tags are ignored by this scan.
\item \texttt{scanonly} - list of tags. Only files that are tagged with any of these tags will be scanned. If \texttt{noscan} is also set the intersection of the two will be scanned.
\item \texttt{envvars} - colon separated list of environment variables that will be set in the scan
\item \texttt{magic} - list of specific identifiers this scan is meant for
\item \texttt{description} - a human readable description of the scan
\item \texttt{xmloutput} - custom output method for XML output of BAT. The code should be in the same file as the actual scan code.
\item \texttt{storetype}, \texttt{storetarget} and \texttt{storedir} - used for postrun scans to describe which files to store in the output, where to store them and where to get them
\end{itemize}
}

\begin{frame}[fragile]
\frametitle{Prerun scans configuration directive example}

\begin{verbatim}
[checkXML]
type        = prerun
module      = bat.prerun
method      = searchXML
priority    = 100
description = Check XML validity
enabled     = yes
\end{verbatim}

\end{frame}

\begin{frame}[fragile]
\frametitle{Unpacking scans configuration directive example}

The following example is for unpacking 7z compressed files. The priority is very low, only the identifier for \texttt{7z} is used, and a bunch of file types can safely be ignored by this scan:

\begin{verbatim}
[7z]
type        = unpack
module      = bat.fwunpack
method      = searchUnpack7z
priority    = 1
magic       = 7z
noscan      = text:xml:graphics:pdf:bz2:gzip:lrzip:
              audio:video
description = Unpack 7z compressed files
enabled     = yes
\end{verbatim}

Note: \texttt{noscan} has been split for readability but is actually one line in the configuration file.
\end{frame}

\begin{frame}[fragile]
\frametitle{File specific scans configuration directive example}
The following example is for querying a database to see if the name of the file is known in package databases of well known distributions. The location of the database is passed as an environment variable:

\begin{verbatim}
[file2package]
type        = program
module      = bat.file2package
method      = filename2package
xmloutput   = xmlprettyprint
envvars     = BAT_PACKAGE_DB=/tmp/filepackages
description = Look up name of file name in popular
              distributions
enabled     = no
\end{verbatim}
Note: \texttt{description} has been split for readability but is actually one line in the configuration file.
\end{frame}

\begin{frame}[fragile]
\frametitle{Postrun scans configuration directive example}
\begin{verbatim}
[hexdump]
type        = postrun
module      = bat.generatehexdump
method      = generateHexdump
noscan      = text:xml:graphics:pdf:audio:video
envvars     = BAT_REPORTDIR=/tmp/images:
              BAT_IMAGE_MAXFILESIZE=100000000
description = Create hexdump output of files
enabled     = no
storetarget = images
storedir    = /tmp/images
storetype   = .png
\end{verbatim}

Note: \texttt{envvars} has been split for readability but is actually one line in the configuration file.
\end{frame}

\frame{
\frametitle{Output the results of the Binary Analysis Tool}
The raw output of the Binary Analysis Tool is written as a tar archive. The tar archive consists of:

\begin{itemize}
\item full directory tree of unpacked files (if any)
\item Python pickles with the results of the scan
\item (optional) pictures with results of the scan
\end{itemize}

The results can be viewed using the Binary Analysis Tool result viewer.

If enabled in the configuration a representation of the results in XML will be printed to standard output after the scan.
}

\frame{
\frametitle{Conclusion}
In this course you have learned about:

\begin{itemize}
\item about the scan order in the Binary Analysis Tool
\item to configure the Binary Analysis Tool
\end{itemize}

In the next course we will see how to view results of the Binary Analysis Tool using the special viewer program.
}
\end{document}
