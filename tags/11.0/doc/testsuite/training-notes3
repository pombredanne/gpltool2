Notes/transcript for presentation.

File: bat-training3.pdf

Slide 1: no notes

Slide 2: In this course we will look at how to configure the Binary Analysis
Tool in detail. For this we first need to look at how BAT works internally.

Slide 3: The Binary Analysis Tool has a few distinct phases. First the binary
is scanned for identifiers that might indicate the start or end of a file
system, compressed file or media file (graphics, audio, video, PDF).

Then each file is inspected to quickly determine a few types, such as finding
if a file is a valid text file, or a valid XML file, and so on, to be able to
quickly filter types and making sure that subsequent scans only get the files
they can actually process.

If the file can be unpacked (file systems, compresed files, media files, etc.)
the file will be unpacked. After that the first three steps will be repeated
for all the unpacked files.

After the files can no not be unpacked any further each file is scanned by file
specific scans and postrun scans.

Slide 4: The identifiers that are searched for are hardcoded in a list in the
source code. The identifiers tell where a file starts or ends. The list can
easily be extended (covered in a next course).

After the identifier search the list of identifiers that were found is passed
to each scan.

Slide 5: The current prerun scans try to determine as quickly as possible what
the type of a file is. The idea is that later scans can ignore files they are
not interested in. For example: a scan to unpack a file system won't work on
a text file or PDF.

Some tags that are currently given are text, binary, xml, graphics, audio, and
elf. Tagging a file only means that a file was positively identified as such,
but the absence of a tag should not be used to draw any conclusions. For
example, UTF-8 files will not be tagged as 'text' since only ASCII files are
tagged as 'text'. Tags are really just an optimisation to avoid unnecessary
scanning later on.

Slide 6: Unpacking scans unpack file systems, compressed files and media files
possibly from a larger file (in case of concatenated data, such as firmwares).
Around 30 file systems, compressed files and media files can currently be
unpacked.

Slide 7: There are quite a few file specific scans, ranging from simple
identifier checks, dynamically linked libraries to more advanced matching
scans.

Slide 8: Postrun scans are scans that simply take the results of previous scans
and process them, but don't change the results, but merely have side effects,
such as generating pictures of the results, or making reports.

Slide 9: The Binary Analysis Tool is extremely configurable: each scan, aprt
from the identifier search, can be enabled or disabled by simply editing a
configuration file. This configuration file is in a very simple Windows INI
format.

The configuration for scanning is conceptually divided in four main parts
(there are a few other configuration sections which do not apply to scanning):
a main part, plus a configuration part per category. Only the general
configuration part is mandatory.

Slide 10: The main configuration defines two things: whether or not BAT should
write the results of a scan in a simple XML format (default: yes) and if BAT
should be run using parallel processing (default: no).

The XML pretty printer can be replaced at will by pointing to a different XML
pretty printer.

Multiprocessing can safely be turned on if there are no scans that have side
effects, such as modifying a database. In the default configuration it is safe
to turn this on.

Slide 11: The configurations for scans are all slightly similar. Depending on
the type of scan there might be a few differences, but each one has a few
mandatory options: type of the scan, the module where the method that
implements the scan can be found, the method name of the scan and if it is
enabled or disabled.

Slide 12: There are also a few optional configuration parameters that can be
set by individual scans. Depending on the type it might, or might not, make
sense in the context. The description option can be set for all scans since it
has no influence. The envvars option can also be set for all scans, but it
depends on the scan whether or not it is supported.

The other scan values are specific per context and it depends on the scan type
and the individual scan whether or not it is implemented.

The noscan parameter can be set for all scans, except prerun scans. Files that
are tagged with a value in this list will be automatically ignored for a
particular scan.

The scanonly parameter can be set for the same scans as noscan can be set for.
Only files that are tagged with a value in this list will be scanned by this
particular scan. If this parameter is omitted or an empty list all files will
be considered for scanning. The noscan parameter takes precedence: if a tag
appears in both, then the file will not be scanned.

The magic parameter can be set for prerun scans and unpack scans. Only files
that have an identifier with any of the identifiers in it will be processed by
the scan.

The priority parameter can be set for prerun scans and unpack scans. This is to
avoid false positives: file systems that do not mangle or compress contents
should be scanned first.

The storetarget, storetype and storedir parameters need to be set together if a
postrun scan creates output (like a report or a picture) that needs to be
stored in the final output.

Slide 13: no comments

Slide 14: This scan has a low priority, is only run for files where the
identifier for 7z can be found in and ignores files which have already been
classified as something that is not 7z.

Slide 15: This scan uses an environment variable, namely the location of a
database with extra information. It is passed to the scan using the envvars
configuration parameter.

Slide 16: Similarly, this scan uses a few environment variables (please note
that in the configuration these should be on one line), and ignores quite a few
file types.

Slide 17: After all scans are done BAT writes its output to a tar file. This
file will contain everything: the original file, the unpacked files, the scan
results as Python pickle files, possibly more output files like pictures and
reports.

If enabled it will also output a simplified XML representation of the scan
results.

Slide 18: no comments
