#############################
### generic configuration ###
#############################

[batconfig]
multiprocessing = yes
module = bat.simpleprettyprint
#module = bat.ppknowledgebase
output = prettyprintresxml
outputlite = no

############################
### viewer configuration ###
############################

[viewer]
htmldir = /home/armijn/html/files
#filter = graphics:text:empty:link

####################
### prerun scans ###
####################

[checkXML]
type        = prerun
module      = bat.prerun
method      = searchXML
priority    = 100
description = Check XML validity
enabled     = yes

[verifyandroiddex]
type        = prerun
module      = bat.prerun
method      = verifyAndroidDex
priority    = 1
description = Check if file is an Android binary XML file
enabled     = yes

[verifyandroidxml]
type        = prerun
module      = bat.prerun
method      = verifyAndroidXML
priority    = 1
description = Check if file is an Android binary XML file
enabled     = yes

[verifybz2]
type        = prerun
module      = bat.prerun
method      = verifyBZ2
priority    = 1
description = Check if complete file is a valid bzip2 file
enabled     = no

[verifyelf]
type        = prerun
module      = bat.prerun
method      = verifyELF
priority    = 2
description = Check if file is a valid ELF library/executable/object
enabled     = yes

[verifygraphics]
type        = prerun
module      = bat.prerun
method      = verifyGraphics
magic       = bmp:jpeg:jpegtrailer:gif87:gif89:png:pngtrailer
priority    = 2
description = Check if file is a valid graphics file, or consists of just graphics files
enabled     = yes

[verifygzip]
type        = prerun
module      = bat.prerun
method      = verifyGzip
priority    = 1
description = Check if complete file is a valid gzip file
enabled     = yes

[verifyjar]
type        = prerun
module      = bat.prerun
method      = verifyJAR
priority    = 1
description = Check if complete file is a valid JAR file
enabled     = no

[verifymessagecatalog]
type        = prerun
module      = bat.prerun
method      = verifyMessageCatalog
priority    = 1
description = Check if file is a valid GNU Message Catalog file
enabled     = yes

[verifymp4]
type        = prerun
module      = bat.prerun
method      = verifyMP4
priority    = 1
description = Check if file is a valid MP4 file
enabled     = no

[verifyogg]
type        = prerun
module      = bat.prerun
method      = verifyOgg
priority    = 1
magic       = ogg
description = Check if file is a valid Ogg file
enabled     = yes

[verifype]
type        = prerun
module      = bat.prerun
method      = verifyPE
priority    = 1
magic       = pe
description = Check if file is a valid PE executable
enabled     = no

[verifytext]
type        = prerun
module      = bat.prerun
method      = verifyText
priority    = 3
description = Check if file contains just ASCII text
enabled     = yes

[verifyttf]
type        = prerun
module      = bat.prerun
method      = verifyTTF
priority    = 1
description = Check if file contains a single TTF font
enabled     = yes

[vimswap]
type        = prerun
module      = bat.prerun
method      = verifyVimSwap
priority    = 1
description = Check if file is a Vim swap file
enabled     = yes

####################
### unpack scans ###
####################

[7z]
type        = unpack
module      = bat.fwunpack
method      = searchUnpack7z
priority    = 1
magic       = 7z
noscan      = text:xml:graphics:pdf:bz2:gzip:lrzip:audio:video
description = Unpack 7z compressed files
enabled     = yes

[ar]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackAr
priority    = 2
magic       = ar
noscan      = text:xml:graphics:pdf:bz2:gzip:lrzip:audio:video
description = Unpack ar archives
enabled     = yes

[arj]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackARJ
priority    = 1
magic       = arj
noscan      = text:xml:graphics:pdf:bz2:gzip:lrzip:audio:video
description = Unpack ARJ compressed files
enabled     = no

[base64]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackBase64
priority    = 0
noscan      = xml:graphics:binary:pdf:compressed:audio:video
description = Decode base64 encoded files
enabled     = yes

[byteswap]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackByteSwap
priority    = 100
noscan      = xml:graphics:pdf:compressed:audio:video
description = Byteswap files for 16 bit flash
enabled     = yes

[bzip2]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackBzip2
priority    = 1
magic       = bz2
noscan      = text:xml:graphics:pdf:gzip:lrzip:audio:video
description = Unpack bzip2 compressed files
enabled     = yes

[cab]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackCab
priority    = 1
magic       = cab
noscan      = text:xml:graphics:pdf:compressed:audio:video
description = Unpack MS Windows Cabinet archives
enabled     = yes

[compress]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackCompress
priority    = 1
magic       = compress
noscan      = text:xml:graphics:pdf:audio:video
description = Unpack files compressed with compress
enabled     = yes

[cpio]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackCpio
priority    = 2
magic       = cpio1:cpio2:cpio3:cpiotrailer
noscan      = text:xml:graphics:pdf:compressed:audio:video
description = Unpack CPIO archives
enabled     = yes

[cramfs]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackCramfs
priority    = 3
magic       = cramfs
noscan      = text:xml:graphics:pdf:compressed:audio:video
description = Unpack cramfs file systems
enabled     = yes

[exe]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackExe
priority    = 2
noscan      = text:xml:graphics:pdf:compressed:audio:video
description = Unpack Microsoft Windows Executable files
enabled     = yes

[ext2fs]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackExt2fs
priority    = 3
magic       = ext2
noscan      = text:xml:graphics:pdf:compressed:audio:video
description = Unpack EXT2/3/4 file systems
enabled     = yes

[gif]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackGIF
priority    = 0
magic       = gif87:gif89
noscan      = text:xml:graphics:pdf:compressed:resource:audio
description = Carve GIF files from a file
enabled     = yes

[gzip]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackGzip
priority    = 1
magic       = gzip
noscan      = text:xml:graphics:pdf:bz2:lrzip:audio:video
description = Unpack gzip compressed files
enabled     = yes

[ico]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackIco
priority    = 0
magic       = ico
noscan      = text:xml:pdf:compressed:graphics:resource:audio:video
description = Carve ICO files from a file
enabled     = no

[installshield]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackInstallShield
priority    = 1
magic       = installshield
noscan      = text:xml:graphics:pdf:bz2:gzip:lrzip:audio:video
description = Unpack InstallShield compressed files
enabled     = yes

[iso9660]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackISO9660
priority    = 4
magic       = iso9660
noscan      = text:xml:graphics:pdf:compressed:audio:video
description = Unpack ISO9660 (CD-ROM) file systems
enabled     = yes

[java_serialized]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackJavaSerialized
priority    = 2
magic       = java_serialized
noscan      = text:xml:graphics:pdf:bz2:gzip:lrzip:audio:video
description = Unpack Java serialized files
enabled     = yes

[jffs2]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackJffs2
priority    = 1
magic       = jffs2_le
noscan      = text:xml:graphics:pdf:compressed:audio:video
description = Unpack JFFS2 file systems
enabled     = yes

[lrzip]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackLRZIP
priority    = 1
magic       = lrzip
noscan      = text:xml:graphics:pdf:bz2:gzip:lzip:audio:video
description = Unpack LRZIP compressed files
enabled     = yes

[lzip]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackLzip
priority    = 1
magic       = lzip
noscan      = text:xml:graphics:pdf:bz2:gzip:lrzip:audio:video
description = Unpack LZIP compressed files
enabled     = yes

[lzma]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackLZMA
priority    = 0
magic       = lzma_alone:lzma_alone_alt
noscan      = text:xml:graphics:pdf:bz2:gzip:lrzip:resource:dalvik:audio:video:elf
description = Unpack LZMA compressed files
envvars     = LZMA_MINIMUM_SIZE=10
enabled     = yes

[lzo]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackLzo
priority    = 1
magic       = lzo
noscan      = text:xml:graphics:pdf:bz2:gzip:lrzip:audio:video
description = Unpack LZO compressed files
enabled     = yes

[minix]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackMinix
priority    = 1
magic       = minix
noscan      = text:xml:graphics:pdf:bz2:gzip:lrzip:audio:video
description = Unpack Minix file systems
enabled     = yes

[pdf_unpack]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackPDF
priority    = 5
magic       = pdf:pdftrailer
noscan      = text:xml:graphics:compressed:audio:video
description = Carve PDF files from a file
enabled     = yes

[png]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackPNG
priority    = 0
magic       = png:pngtrailer
noscan      = text:xml:pdf:compressed:graphics:resource:audio
description = Carve PNG files from a file
enabled     = yes

[rar]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackRar
priority    = 1
magic       = rar
noscan      = text:xml:graphics:pdf:bz2:gzip:lrzip:audio:video
description = Unpack RAR archives
enabled     = yes

[romfs]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackRomfs
priority    = 2
magic       = romfs
noscan      = text:xml:graphics:pdf:bz2:gzip:lrzip:audio:video
description = Unpack romfs file systems
enabled     = yes

[rpm]
type        = unpack
module      = bat.unpackrpm
method      = searchUnpackRPM
priority    = 2
magic       = rpm:gzip:xz:xztrailer
noscan      = text:xml:graphics:pdf:compressed:audio:video
description = Unpack RPM files
enabled     = yes

[squashfs]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackSquashfs
priority    = 1
magic       = squashfs1:squashfs2:squashfs3:squashfs4:squashfs5:squashfs6:squashfs7
noscan      = text:xml:graphics:pdf:compressed:audio:video
description = Unpack squashfs file systems
enabled     = yes

[swf]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackSwf
priority    = 0
magic       = swf
noscan      = text:xml:pdf:compressed:graphics:resource:audio:video
description = Unpack compressed Shockwave Flash files
enabled     = yes

[tar]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackTar
priority    = 6
magic       = tar1:tar2
noscan      = text:xml:graphics:pdf:bz2:gzip:lrzip:audio:video
description = Unpack tar archives
enabled     = yes

###### DO NOT USE UNLESS YOU HAVE the "unubi" tool!
[ubifs]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackUbifs
priority    = 3
magic       = ubifs
noscan      = text:xml:graphics:pdf:compressed:audio:video
description = Unpack UBI file systems
enabled     = no

[upx]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackUPX
priority    = 3
noscan      = text:xml:graphics:pdf:compressed:audio:video
description = Unpack UPX compressed executables
enabled     = yes

#[xor]
#type        = unpack
#module      = bat.batxor
#method      = searchUnpackXOR
#priority    = 10
#noscan      = xml:graphics:pdf:compressed:audio:video:elf:temporary
#scanonly    = binary
#description = XOR 'decryption'
#enabled     = no

[xz]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackXZ
priority    = 1
magic       = xz:xztrailer
noscan      = text:xml:graphics:pdf:bz2:gzip:lrzip:audio:video
description = Unpack XZ compressed files
enabled     = yes

[yaffs2]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackYaffs2
priority    = 2
noscan      = text:xml:graphics:pdf:compressed:audio:video
description = Unpack YAFFS2 file systems
enabled     = yes

[zip]
type        = unpack
module      = bat.fwunpack
method      = searchUnpackZip
priority    = 1
magic       = zip
noscan      = text:xml:graphics:pdf:bz2:gzip:lrzip:audio:video
description = Unpack ZIP compressed files
enabled     = yes

##################
### leaf scans ###
##################

[architecture]
type        = program
module      = bat.checks
method      = scanArchitecture
noscan      = text:xml:graphics:pdf:compressed:audio:video
description = Scan executable file architecture
enabled     = yes

[busybox-version]
type        = program
module      = bat.busyboxversion
method      = busybox_version
noscan      = text:xml:graphics:pdf:compressed:audio:video
description = Extract BusyBox version number
enabled     = yes

[dproxy]
type        = program
module      = bat.checks
method      = searchDproxy
noscan      = text:xml:graphics:pdf:compressed:audio:video
description = Determine presence of dproxy
enabled     = yes

[ez-ipupdate]
type        = program
module      = bat.checks
method      = searchEzIpupdate
noscan      = text:xml:graphics:pdf:compressed:audio:video
description = Determine presence of ez-update
enabled     = yes

[file2package]
type        = program
module      = bat.file2package
method      = filename2package
xmloutput   = xmlprettyprint
envvars     = BAT_PACKAGE_DB=/tmp/filepackages
description = Look up name of file name in popular distributions
enabled     = no

[forges]
type        = program
module      = bat.checks
method      = scanForges
xmloutput   = forgesPrettyPrint
noscan      = graphics:compressed:audio:video
description = Scan for presence of markers of forges/collaborative software development sites
enabled     = yes

[iproute]
type        = program
module      = bat.checks
method      = searchIproute
noscan      = text:xml:graphics:pdf:compressed:audio:video
description = Determine presence of iproute
enabled     = yes

[iptables]
type        = program
module      = bat.checks
method      = searchIptables
noscan      = text:xml:graphics:pdf:compressed:audio:video
description = Determine presence of iptables
enabled     = yes

[kernelchecks]
type        = program
module      = bat.kernelanalysis
method      = kernelChecks
xmloutput   = xmlprettyprint
noscan      = text:xml:graphics:pdf:compressed:audio:video
description = Detect version number and some subsystems in Linux kernel
enabled     = yes

[kernelmodulelicense]
type        = program
module      = bat.kernelanalysis
method      = analyseModuleLicense
noscan      = text:xml:graphics:pdf:compressed:audio:video
description = Determine declared Linux kernel module license
enabled     = yes

[libs]
type        = program
module      = bat.checks
method      = searchDynamicLibs
xmloutput   = dynamicLibsPrettyPrint
noscan      = text:xml:graphics:pdf:compressed:audio:video
description = Determine dynamically linked libraries
enabled     = yes

[libusb]
type        = program
module      = bat.checks
method      = searchLibusb
noscan      = text:xml:graphics:pdf:compressed:audio:video
description = Determine presence of libusb
enabled     = yes

[licenses]
type        = program
module      = bat.checks
method      = scanLicenses
xmloutput   = licensesPrettyPrint
noscan      = compressed
description = Scan for presence of license markers
enabled     = yes

[loadlin]
type        = program
module      = bat.checks
method      = searchLoadLin
noscan      = text:xml:graphics:pdf:compressed:audio:video
description = Determine presence of libusb
enabled     = yes

[pdf]
type        = program
module      = bat.checks
method      = scanPDF
xmloutput   = pdfPrettyPrint
noscan      = text:xml:graphics:compressed:audio:video
description = Determine characteristics of PDF files
enabled     = yes

[ranking]
type        = program
module      = bat.ranking
method      = searchGeneric
xmloutput   = xmlprettyprint
envvars     = BAT_AVG_C=/gpl/master/avg_c:BAT_STRINGSCACHE_C=/gpl/master/stringscache_c:BAT_AVG_JAVA=/gpl/master/avg_java:BAT_STRINGSCACHE_JAVA=/gpl/master/stringscache_java:BAT_DB=/gpl/master/master.sqlite3:BAT_FUNCTIONNAMECACHE_C=/gpl/master/functioncache_c:BAT_FUNCTIONNAMECACHE_JAVA=/gpl/master/functioncache_java:BAT_RANKING_FULLCACHE=1:BAT_RANKING_LICENSE=1:BAT_RANKING_VERSION=1:BAT_CLONE_DB=/tmp/clonedb:BAT_LICENSE_DB=/gpl/master/licenses.sqlite3
noscan      = text:xml:graphics:pdf:compressed:resource:audio:video:vimswap
description = Classify packages using advanced ranking mechanism
enabled     = no
parallel    = no

[redboot]
type        = program
module      = bat.checks
method      = searchRedBoot
noscan      = text:xml:graphics:pdf:compressed:audio:video
description = Determine presence of RedBoot
enabled     = yes

[uboot]
type        = program
module      = bat.checks
method      = searchUBoot
noscan      = text:xml:graphics:pdf:compressed:audio:video
description = Determine presence of U-Boot
enabled     = yes

[vsftpd]
type        = program
module      = bat.checks
method      = searchVsftpd
noscan      = text:xml:graphics:pdf:compressed:audio:video
description = Determine presence of vsftpd
enabled     = yes

[wireless-tools]
type        = program
module      = bat.checks
method      = searchWirelessTools
noscan      = text:xml:graphics:pdf:compressed:audio:video
description = Determine presence of wireless-tools
enabled     = yes

[wpa-supplicant]
type        = program
module      = bat.checks
method      = searchWpaSupplicant
noscan      = text:xml:graphics:pdf:compressed:audio:video
description = Determine presence of wpa-supplicant
enabled     = yes

#######################
### aggregate scans ###
#######################

[findlibs]
type        = aggregate
module      = bat.findlibs
method      = findlibs
noscan      = text:xml:graphics:pdf:audio:video
enabled     = no

[jars]
type        = aggregate
module      = bat.aggregatejars
method      = aggregatejars
noscan      = text:xml:graphics:pdf:audio:video
enabled     = yes

#####################
### postrun scans ###
#####################

[hexdump]
type        = postrun
module      = bat.generatehexdump
method      = generateHexdump
noscan      = text:xml:graphics:pdf:audio:video
envvars     = BAT_REPORTDIR=/tmp/images:BAT_IMAGE_MAXFILESIZE=100000000
description = Create hexdump output of files
enabled     = no
storetarget = reports
storedir    = /tmp/images
storetype   = -hexdump.gz

[images]
type        = postrun
module      = bat.images
method      = generateImages
noscan      = text:xml:graphics:compressed:pdf:audio:video:resources
envvars     = BAT_IMAGEDIR=/tmp/images:BAT_IMAGE_MAXFILESIZE=100000000
description = Generate graphical representation of files
enabled     = no
storetarget = images
storedir    = /tmp/images
storetype   = .png


[matchednames]
type        = postrun
module      = bat.generatenameshtml
method      = generateHTML
noscan      = text:xml:graphics:pdf:audio:video
envvars     = BAT_REPORTDIR=/tmp/images
description = Generate HTML files with matched names (functions, methods, variables, fields, etc.) for use in GUI
enabled     = no
storetarget = reports
storedir    = /tmp/images
storetype   = -names.html.gz:-functionnames.html.gz
cleanup     = yes

[rankimages]
type        = postrun
module      = bat.rankimages
method      = generateImages
noscan      = text:xml:graphics:pdf:audio:video
envvars     = BAT_IMAGEDIR=/tmp/images
description = Generate images of results of ranking
enabled     = no
storetarget = images
storedir    = /tmp/images
storetype   = -piechart.png:-version.png:-funcversion.png
cleanup     = yes

[uniquehtml]
type        = postrun
module      = bat.generateuniquehtml
method      = generateHTML
noscan      = text:xml:graphics:pdf:audio:video
envvars     = BAT_REPORTDIR=/tmp/images
description = Generate HTML files with links to pretty printed source code for matched and unmatched strings
enabled     = no
storetarget = reports
storedir    = /tmp/images
storetype   = -unique.html.gz:-unmatched.html.gz
cleanup     = yes
