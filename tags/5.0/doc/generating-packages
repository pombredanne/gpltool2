HOWTO: Generating BAT packages for your distribution

If you want to install BAT through the package manager of your distribution you might first need to generate packages for it if none exist. For BAT there is currently support to build packages for RPM-based systems and for DEB-based systems.

  Building packages for RPM based systems

1. Make a fresh checkout from Subversion
2. run the command:
     python setup.py bdist_rpm

This will create a few files: an RPM file and an SRPM file. The RPM file will typically only work on the distribution you built it on, since several dependencies and paths (like the Python version) will be hardcoded in the RPM. If you need to install it on various distributions you can rebuild the SRPM.


  Building packages for DEB based systems

The Debian scripts were written according to the documentation for debhelper found here:

https://wiki.ubuntu.com/PackagingGuide/Python

Package development is done on Ubuntu 10.10.

To build a .deb package do a checkout of Subversion. Change to the directory 'src' and type:

debuild -uc -us

It will complain about not being able to find the original sources. You can ignore this, until we have a cleaner solution. The command will build a .deb package which you can install with dpkg -i. You might have to take care of installing some dependencies using apt-get first.
